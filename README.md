# Demo for the Serverless Architecture Conference

https://serverless-architecture.io/

April 8 – 10, 2019

The Hague | Netherlands

## Setup Instructions

1. Create a new project
  - Go to https://gitlab.com/projects/new
  - Name the project `sac-demo`
  - Select `Public` visibility
  - Check to `Initialize repository with a README`
  - Click `Create Project`

2. Create a cluster
  - Go to Operations > Kubernetes
  - Click `Add Kubernetes Cluster`
  - Sign in with Google
  - Give the cluster a name
  - Select a project
  - Select a Zone
  - Number of Nodes (demo can run with 1)
  - Click `Create Kubernetes cluster`
  - The GKE Cluster takes 5-10 minutes to initialize

3. Install Apps
  - Click to install Helm
  - Refresh the page (Current UX bug, this will make the `Knative Domain Name` field appear)
  - Add you domain to the `Knative Domain Name` field and install Knative (e.g. `sac-demo.williamchia.me`)
  - Copy the Knative Enpoint IP Address
  - Create a wildcard DNS entry
  - Type: A, Host: `*.sac-demo`, IP: `IP_ADDRESS`, TTL: 600

## Create Function Demo

Create files manually, or fork https://gitlab.com/knative-examples/functions

1. Add a new directory called `echo`
2. In the echo directory, create `echo.js`

### echo.js

```
var exports = module.exports = function(name) {
  name = JSON.stringify(name);
  return name
}
```
3. Create `package-lock.json`, and `package.json`

On your local machine run the following commands

```
cd echo
npm init #take all the defaults
npm install
```

### package-lock.json

```
{
  "name": "runtime-nodejs-example-module",
  "version": "1.0.0",
  "lockfileVersion": 1
}
```

### package.json

```
{
  "name": "runtime-nodejs-example-module",
  "version": "1.0.0",
  "description": "",
  "main": "echo.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": ""
}
```

4. At the root level create `.gitlab-ci.yml`  and `serverless.yml`

### .gitlab-ci.yml

```
include:
  template: Serverless.gitlab-ci.yml

.serverless:build:functions:
  stage: build
  environment: development
  image: registry.gitlab.com/gitlab-org/gitlabktl:latest
  script: /usr/bin/gitlabktl serverless build

.serverless:deploy:functions:
  stage: deploy
  environment: development
  image: registry.gitlab.com/gitlab-org/gitlabktl:latest
  script: /usr/bin/gitlabktl serverless deploy

functions:build:
  extends: .serverless:build:functions
  environment: production

functions:deploy:
  extends: .serverless:deploy:functions
  environment: production
```
### serverless.yml

```
service: functions
description: "Deploying functions from GitLab using Knative"

functions:
  echo-js:
    handler: echo
    source: ./echo
    runtime: https://gitlab.com/gitlab-org/serverless/runtimes/nodejs
    description: "node.js runtime function"
    environment:
      MY_FUNCTION: echo
```

5. Use the function
  - Go to Operations > Serverless
  - Copy the function URL
  - paste into this curl command and run locally

```
curl \
--header "Content-Type: application/json" \
--request POST \
--data '{"GitLab":"FaaS"}' \
<paste_url_here>
```

## Create unit test demo

1. Add a `.gitignore`

```
node_modules
```

2. Install mocha

On your local machine run the following commands in the root directory

```
npm install mocha
npm init #take all the defaults
```

3. Update the pipeline with a test stages

Edit `.gitlab-ci.yml` and add the following lines

```
stages:
  - build
  - test
  - deploy

test:
  stage: test
  image: node:latest
  script:
    - npm install
    - ./node_modules/mocha/bin/mocha echo
```

4. Add a test

In the echo directory, create test.js

```
var echo = require('./echo');
var assert = require('assert');

describe('test case', function() {
    it('message', function(done){
        var message = "Hello from GitLab Serverless";
        assert.equal(JSON.stringify(message), echo(message));
        done();
    })
});
```

5. Commit your code and the pipeline will run

## Links

1. GitLab Serverless: https://about.gitlab.com/product/serverless/
2. GitLab Serverless Docs: https://docs.gitlab.com/ee/user/project/clusters/serverless/
3. GitLab Serverless Runtimes: https://gitlab.com/gitlab-org/serverless/runtimes/
3. Serverless.yml Template: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Serverless.gitlab-ci.yml
4. GitLab Serverless CI YAML template: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Serverless.gitlab-ci.yml
5. Knative Docs example: https://github.com/knative/docs/tree/master/docs/serving/samples/hello-world/helloworld-nodejs
6. Knative Event Sources: https://github.com/knative/docs/tree/master/docs/eventing/sources
7. GitLab Knative examples: https://gitlab.com/knative-examples
8. GitLab CI Docs: https://docs.gitlab.com/ee/ci/yaml/README.html 



